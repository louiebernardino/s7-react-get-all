import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';


const TeamRow = ({teamAttr, index}) => {
  return (
  	<Fragment>
  		<tr>
          <th scope="row">{ index }</th>
          <td>{ teamAttr.name }</td>
          <td>adamg</td>
          <td>adamg@gmail.com</td>
          <td>Admin</td>
          <td><Button size="sm" color="primary"><i className="fas fa-eye"></i></Button>
          <Button size="sm" className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button size="sm" color="danger"><i className="fas fa-trash-alt"></i></Button></td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>B</td>
          <td>emmag</td>
          <td>emmag@gmail.com</td>
          <td>Student</td>
          <td><Button size="sm" color="primary"><i className="fas fa-eye"></i></Button>
          <Button size="sm"  className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button size="sm" color="danger"><i className="fas fa-trash-alt"></i></Button></td>
         </tr>
    </Fragment>
	);
}

export default TeamRow;

