//Dependencies
import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';


//COMPONENTS
const MembersPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
console.log("Member", props.getCurrentMemberAttr)
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Members Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><MemberForm/></Col>
        <Col><MemberTable/></Col>
      </Row>
    </Container>
  );
}

export default MembersPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)