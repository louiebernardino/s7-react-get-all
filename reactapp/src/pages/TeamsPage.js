//Dependencies
import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import TeamForm from '../forms/TeamForm';
import TeamRow from '../rows/TeamRow';
import TeamTable from '../tables/TeamTable';
import axios from 'axios';


//COMPONENTS
const TeamsPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
const [ teamsData, setTeamsDate] = useState({
  token: props.tokenAttr,
  teams: []
})

const { token, teams} = teamsData
console.log("Teams Component", teams)

const getTeams = async () => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.get("http://localhost:4051/teams", config)

    console.log("Teams Res", res)
    return setTeamsDate({
      teams: res.data
    })

  } catch(e) {
    console.log(e.response)
    //swal
  }

}

//getTeams()
useEffect(()=> {
  getTeams()
}, [setTeamsDate])

console.log("Team", props.getCurrentMemberAttr)
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Teams Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><TeamForm/></Col>
        <Col><TeamTable teamsAttr={teams}/></Col>
      </Row>
    </Container>
  );
}

export default TeamsPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)